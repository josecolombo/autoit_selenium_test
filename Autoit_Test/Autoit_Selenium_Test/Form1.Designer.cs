﻿
namespace Autoit_Selenium_Test
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStartWebScraper = new System.Windows.Forms.Button();
            this.txbSearch = new System.Windows.Forms.TextBox();
            this.lblSearch = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblFileName = new System.Windows.Forms.Label();
            this.txbFileName = new System.Windows.Forms.TextBox();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.txbFilePath = new System.Windows.Forms.TextBox();
            this.btnSelectPath = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStartWebScraper
            // 
            this.btnStartWebScraper.Location = new System.Drawing.Point(250, 231);
            this.btnStartWebScraper.Name = "btnStartWebScraper";
            this.btnStartWebScraper.Size = new System.Drawing.Size(97, 36);
            this.btnStartWebScraper.TabIndex = 4;
            this.btnStartWebScraper.Text = "Start";
            this.btnStartWebScraper.UseVisualStyleBackColor = true;
            this.btnStartWebScraper.Click += new System.EventHandler(this.btnStartWebScraper_Click);
            // 
            // txbSearch
            // 
            this.txbSearch.Location = new System.Drawing.Point(15, 34);
            this.txbSearch.Name = "txbSearch";
            this.txbSearch.Size = new System.Drawing.Size(232, 20);
            this.txbSearch.TabIndex = 0;
            this.txbSearch.Text = "RHITMO Tech";
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(12, 18);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(132, 13);
            this.lblSearch.TabIndex = 2;
            this.lblSearch.Text = "Realizar busca no Google:";
            // 
            // lblError
            // 
            this.lblError.Location = new System.Drawing.Point(15, 270);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(332, 57);
            this.lblError.TabIndex = 3;
            // 
            // lblFileName
            // 
            this.lblFileName.AutoSize = true;
            this.lblFileName.Location = new System.Drawing.Point(12, 62);
            this.lblFileName.Name = "lblFileName";
            this.lblFileName.Size = new System.Drawing.Size(109, 13);
            this.lblFileName.TabIndex = 4;
            this.lblFileName.Text = "Nome do documento:";
            // 
            // txbFileName
            // 
            this.txbFileName.Location = new System.Drawing.Point(15, 78);
            this.txbFileName.Name = "txbFileName";
            this.txbFileName.Size = new System.Drawing.Size(232, 20);
            this.txbFileName.TabIndex = 1;
            this.txbFileName.Text = "Search";
            // 
            // lblFilePath
            // 
            this.lblFilePath.AutoSize = true;
            this.lblFilePath.Location = new System.Drawing.Point(15, 105);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(122, 13);
            this.lblFilePath.TabIndex = 5;
            this.lblFilePath.Text = "Caminho do documento:";
            // 
            // txbFilePath
            // 
            this.txbFilePath.Location = new System.Drawing.Point(15, 121);
            this.txbFilePath.Name = "txbFilePath";
            this.txbFilePath.Size = new System.Drawing.Size(232, 20);
            this.txbFilePath.TabIndex = 3;
            // 
            // btnSelectPath
            // 
            this.btnSelectPath.Location = new System.Drawing.Point(253, 121);
            this.btnSelectPath.Name = "btnSelectPath";
            this.btnSelectPath.Size = new System.Drawing.Size(32, 20);
            this.btnSelectPath.TabIndex = 2;
            this.btnSelectPath.Text = "...";
            this.btnSelectPath.UseVisualStyleBackColor = true;
            this.btnSelectPath.Click += new System.EventHandler(this.btnSelectPath_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 336);
            this.Controls.Add(this.btnSelectPath);
            this.Controls.Add(this.txbFilePath);
            this.Controls.Add(this.lblFilePath);
            this.Controls.Add(this.txbFileName);
            this.Controls.Add(this.lblFileName);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.txbSearch);
            this.Controls.Add(this.btnStartWebScraper);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartWebScraper;
        private System.Windows.Forms.TextBox txbSearch;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label lblFileName;
        private System.Windows.Forms.TextBox txbFileName;
        private System.Windows.Forms.Label lblFilePath;
        private System.Windows.Forms.TextBox txbFilePath;
        private System.Windows.Forms.Button btnSelectPath;
    }
}

