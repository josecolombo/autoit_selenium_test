﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AutoItX3Lib;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Autoit_Selenium_Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStartWebScraper_Click(object sender, EventArgs e)
        {
            string textToSearch = txbSearch.Text;
            string fileName = txbFileName.Text;
            string path = txbFilePath.Text;
            WriteNotepad(WebScrapringOnGoogle(textToSearch), fileName, path);
        }

        private string WebScrapringOnGoogle(string search)
        {
            ChromeDriver driver = new ChromeDriver();

            driver.Navigate().GoToUrl("https://google.com");

            driver.Manage().Window.Maximize();

            driver.FindElement(By.XPath("//input[@name=\"q\"]")).SendKeys(search);
            driver.FindElement(By.XPath("//input[@name=\"q\"]")).SendKeys(OpenQA.Selenium.Keys.Enter);

            string SearchReturn = driver.FindElement(By.Id("search")).Text;

            driver.Quit();

            return SearchReturn;
        }

        private void WriteNotepad(string textToWrite, string fileName, string path)
        {
            AutoItX3 autoit = new AutoItX3();

            autoit.Run("notepad.exe");

            //Adicionar verificação de idioma!
            autoit.WinWaitActive("Sem título - Bloco de Notas");

            autoit.Send(textToWrite);

            autoit.WinClose("*Sem título - Bloco de Notas"); //Atentar-se ao * quando o arquivo é editado!
            autoit.WinWaitActive("Bloco de notas", "Salvar");
            autoit.Send("{ENTER}");

            string fullPath = ChecksFileExists_ReturnFullPath(fileName, path);

            autoit.Send(fullPath);

            autoit.Send("{ENTER}");
            //autoit.WinKill(winHandle);
        }

        private string ChecksFileExists_ReturnFullPath(string fileName, string path)
        {
            int countFiles = 0;
            string fullPath = string.IsNullOrEmpty(path) ? AppContext.BaseDirectory + $@"{fileName}.txt" : path + $@"\{fileName}.txt";
            while (File.Exists(fullPath))
            {
                fileName = fileName.Contains($"({countFiles})") ? fileName.Replace($"({countFiles})", "") : fileName;

                ++countFiles;
                fileName = fileName + $"({countFiles})";
                fullPath = string.IsNullOrEmpty(path) ? AppContext.BaseDirectory + $@"{fileName}.txt" : path + $@"\{fileName}.txt";
            }

            return fullPath;
        }

        private void btnSelectPath_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            txbFilePath.Text = dialog.SelectedPath;
        }
    }
}
